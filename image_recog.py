from PIL import Image
from pytesseract import pytesseract
import re
import tkinter as tk
from tkinter import ttk

def image_processed(path):
    # Defining paths to tesseract.exe
    # and the image we would be using
    path_to_tesseract = r"C:\Program Files\Tesseract-OCR\tesseract.exe"

    # Providing the tesseract executable
    # location to pytesseract library
    pytesseract.tesseract_cmd = path_to_tesseract
    img = Image.open(path)
    ext = path[len(path) - 3:]
    if ext=='jpg':
        path = path[0:len(path) - 4]+"."+"png"
        img.save(path)

    img = img.convert('RGBA')
    pix = img.load()

    for y in range(img.size[1]):
        for x in range(img.size[0]):
           if pix[x, y][0] < 102 or pix[x, y][1] < 102 or pix[x, y][2] < 102:
              pix[x, y] = (0, 0, 0, 255)
           else:
              pix[x, y] = (255, 255, 255, 255)

    img.save(path)

    text = pytesseract.image_to_string(Image.open(path))
    return text

def aadhar(path):
    #  Initializing data variable
    name = None
    gender = None
    year = None
    uid = None

    # Searching for Year of Birth
    lines = image_processed(path)
    # print (lines)
    name = lines.split('\n')[1]
    for wordlist in lines.split('\n'):
        xx = wordlist.split()

        if [w for w in xx if re.search('(Year|Birth|irth|YoB|YOB:|DOB:|DOB)$', w)]:
            year = xx
        if [w for w in xx if re.search('(Female|Male|emale|male|ale|FEMALE|MALE|EMALE)$', w)]:
            gender = xx[len(xx) - 1]
    # for wordlist in lines.split('\n'):
    # print(wordlist)
    for wordlist in lines.split('\n'):
        if re.search('^[2-9]{1}[0-9]{3}\\s[0-9]{4}\\s[0-9]{4}$', wordlist):
            uid = wordlist

    root = tk.Tk()
    T = tk.Text(root, height=100, width=300)
    root.geometry("1600x900")
    Fact = '''         *******************************************************\n\n
                           AADHAR DETAILS                      \n\n
        *******************************************************\n\n
        NAME  ''' + lines.split('\n')[5] + '''
        -------------------------------------------------------\n\n
        DOB  ''' + year[year.index('DOB:') + 1] + '''
        -------------------------------------------------------\n\n
        GENDER  ''' + gender + '''
        -------------------------------------------------------\n\n
        AADHAR NUMBER  ''' + uid + '''
        *******************************************************\n\n'''
    T.pack()
    T.insert(tk.END, Fact)
    tk.mainloop()



def driver_licence(path):
    lines = image_processed(path)
    word = lines.split('\n')

    root = tk.Tk()
    T = tk.Text(root, height=100, width=300)
    root.geometry("1600x900")
    Fact = '''          *******************************************************\n\n
                      REQUIRED DRIVER'S DETAILS            \n\n
    *******************************************************\n\n
    NUMBER PLATE  '''+ word[2].split()[2]+" "+word[2].split()[3]+'''
    -------------------------------------------------------\n\n
    VALID FROM  '''+word[5].split()[0]+'''
    -------------------------------------------------------\n\n
    VALID TILL  '''+word[5].split()[2]+'''
    -------------------------------------------------------\n\n
    DOB  '''+word[10].split()[0]+'''
    *******************************************************\n\n'''
    T.pack()
    T.insert(tk.END, Fact)
    tk.mainloop()



def pan_card(path):
    lines = image_processed(path)
    word = lines.split('\n')

    root = tk.Tk()
    T = tk.Text(root, height=100, width=300)
    root.geometry("1600x900")
    Fact = '''       *******************************************************\n\n
                    PAN CARD DETAILS                       \n\n
    *******************************************************\n\n

    NAME :'''+ word[4].split()[0] +" "+ word[4].split()[1]+'''
    -------------------------------------------------------\n\n
    DOB :'''+word[8].split()[0]+'''
    -------------------------------------------------------\n\n
    NUMBER :'''+word[12].split()[0]+'''
    -------------------------------------------------------\n\n
    REGIONAL :'''+word[16].split()[0]+'''
    *******************************************************\n\n'''
    T.pack()
    T.insert(tk.END, Fact)
    tk.mainloop()

def submit():
    name = file_name.get()
    file_name.set("")
    card = card_name.get()
    if card == 'AADHAR':
        aadhar(name)

    elif card == 'PANCARD':
        pan_card(name)
    else:
        driver_licence(name)



root = tk.Tk()
root.geometry("1000x1000")
file_name = tk.StringVar()
card_name = tk.StringVar()

file_label = tk.Label(root, text='FILE NAME', font=('Times New Roman', 20, 'bold'),fg="red").place(x=0,y=220)
file_entry = tk.Entry(root, textvariable=file_name, font=('Times New Roman', 20, 'normal'),fg='green').place(x=131,y=280)
sub_btn=tk.Button(root,text = 'SUBMIT', command = submit,fg='white',bg='green', font=('calibre', 20, 'bold')).place(x=330,y=345)


# label
tk.Label(root, text="SELECT A CARD",
          font=("Times New Roman", 20,'bold'),fg="red").grid(column=0,
                                             row=5, padx=10, pady=25)

# Combobox creation

cd = ttk.Combobox(root, width=27, textvariable=card_name,font=("Times New Roman", 20))

# Adding combobox drop down list
cd['values'] = ('AADHAR',
                'PANCARD',
                'DRIVER\'S LISENCE')

cd.place(x=131,y=90)
cd.current(0)
root.mainloop()

